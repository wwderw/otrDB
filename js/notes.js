// Sell notes

const sellAdder = document.querySelector('.sellAdder');
const mySells = document.querySelector('#sellID');
const sells = JSON.parse(localStorage.getItem('sellShows')) || [];


sellAdder.addEventListener('submit', addTask);
mySells.addEventListener('click',toggleDone);
outputSells();



function addTask(e){
    e.preventDefault();
    const textQuote = this.querySelector('[name=sell]').value;
    const sell = {
       textQuote, done:false

    }
    sells.push(sell);
    saveStorage();
    outputSells();
    this.reset();
}

function saveStorage() {
    localStorage.setItem('sellShows', JSON.stringify(sells));
}

function outputSells(){
  let html = sells.map(function (data, i){
    let myClass = data.done ? 'done':'';
    return '<li div data-index=' + i + '><div class="'+ myClass + '">'+ data.textQuote + '<span class="remove"> X </span></div></li>';
  })
  mySells.innerHTML = html.join('');
}

function toggleDone(e) {
    //console.log(e);
    const myEl = e.target;
    const mySel = myEl.parentElement;
    if (myEl.className === 'remove') {
        let index = mySel.parentElement.dataset.index;
        let temp = sells.splice(index, 1);
        console.log(temp);
    }
    else {
        //console.log(e);
        // console.log(tasks[mySel ]);
        myEl.classList.toggle('done');
        //console.log(myEl);
        sells[mySel.dataset.index].done = !sells[mySel.dataset.index].done;
    }
    saveStorage();
    //console.log(tasks);
    outputSells();
}

// Shows to be on the lookout

const collectAdder = document.querySelector('.collectAdder');
const myCollection = document.querySelector('#collectID');
const collecting = JSON.parse(localStorage.getItem('lookoutShows')) || [];


collectAdder.addEventListener('submit', addToSell);
myCollection.addEventListener('click',toggleDoneSell);
outputCollect();



function addToSell(e){
    e.preventDefault();
    const textQuote = this.querySelector('[name=collect]').value;
    const collect = {
       textQuote, done:false

    }
    collecting.push(collect);
    saveStorageSells();
    outputCollect();
    this.reset();
}

function saveStorageSells() {
    localStorage.setItem('lookoutShows', JSON.stringify(collecting));
}

function outputCollect(){
  let html = collecting.map(function (data, i){
    let myClass = data.done ? 'done':'';
    return '<li div data-index=' + i + '><div class="'+ myClass + '">'+ data.textQuote + '<span class="remove"> X </span></div></li>';
  })
  myCollection.innerHTML = html.join('');
}

function toggleDoneSell(e) {
    //console.log(e);
    const myEl = e.target;
    const mySel = myEl.parentElement;
    if (myEl.className === 'remove') {
        let index = mySel.parentElement.dataset.index;
        let temp = collecting.splice(index, 1);
        console.log(temp);
    }
    else {
        //console.log(e);
        // console.log(tasks[mySel ]);
        myEl.classList.toggle('done');
        //console.log(myEl);
        collecting[mySel.dataset.index].done = !collecting[mySel.dataset.index].done;
    }
    saveStorageSells();
    //console.log(tasks);
    outputCollect();
}

// Nots for Shows to be needDigitized

const needAdder = document.querySelector('.needAdder');
const myToDigitizing = document.querySelector('#todoID');
const needToDigitizing = JSON.parse(localStorage.getItem('needToDigitize')) || [];


needAdder.addEventListener('submit', addToDigitize);
myToDigitizing.addEventListener('click',toggleDoneToDigitize);
outputToDigitize();



function addToDigitize(e){
    e.preventDefault();
    const textQuote = this.querySelector('[name=toDigitize]').value;
    const toDigitize = {
       textQuote, done:false

    }
    needToDigitizing.push(toDigitize);
    saveStorageToDigitize();
    outputToDigitize();
    this.reset();
}

function saveStorageToDigitize() {
    localStorage.setItem('needToDigitize', JSON.stringify(needToDigitizing));
}

function outputToDigitize(){
  let html = needToDigitizing.map(function (data, i){
    let myClass = data.done ? 'done':'';
    return '<li div data-index=' + i + '><div class="'+ myClass + '">'+ data.textQuote + '<span class="remove"> X </span></div></li>';
  })
  myToDigitizing.innerHTML = html.join('');
}

function toggleDoneToDigitize(e) {
    //console.log(e);
    const myEl = e.target;
    const mySel = myEl.parentElement;
    if (myEl.className === 'remove') {
        let index = mySel.parentElement.dataset.index;
        let temp = needToDigitizing.splice(index, 1);
        console.log(temp);
    }
    else {
        //console.log(e);
        // console.log(tasks[mySel ]);
        myEl.classList.toggle('done');
        //console.log(myEl);
        needToDigitizing[mySel.dataset.index].done = !needToDigitizing[mySel.dataset.index].done;
    }
    saveStorageToDigitize();
    //console.log(tasks);
    outputToDigitize();
}

// Already Processed Shows Notes

const doneAdder = document.querySelector('.processAdder');
const myDoneDigitizing = document.querySelector('#processID');
const alreadyDoneDigitizing = JSON.parse(localStorage.getItem('doneDigitize')) || [];


doneAdder.addEventListener('submit', alreadyDigitize);
myDoneDigitizing.addEventListener('click',toggleDoneDigitized);
outputDoneDigitize();



function alreadyDigitize(e){
    e.preventDefault();
    const textQuote = this.querySelector('[name=doneDigitize]').value;
    const doneDigitize = {
       textQuote, done:false

    }
    alreadyDoneDigitizing.push(doneDigitize);
    saveStorageDoneDigitize();
    outputDoneDigitize();
    this.reset();
}

function saveStorageDoneDigitize() {
    localStorage.setItem('doneDigitize', JSON.stringify(alreadyDoneDigitizing));
}

function outputDoneDigitize(){
  let html = alreadyDoneDigitizing.map(function (data, i){
    let myClass = data.done ? 'done':'';
    return '<li div data-index=' + i + '><div class="'+ myClass + '">'+ data.textQuote + '<span class="remove"> X </span></div></li>';
  })
  myDoneDigitizing.innerHTML = html.join('');
}

function toggleDoneDigitized(e) {
    //console.log(e);
    const myEl = e.target;
    const mySel = myEl.parentElement;
    if (myEl.className === 'remove') {
        let index = mySel.parentElement.dataset.index;
        let temp = alreadyDoneDigitizing.splice(index, 1);
        console.log(temp);
    }
    else {
        //console.log(e);
        // console.log(tasks[mySel ]);
        myEl.classList.toggle('done');
        //console.log(myEl);
        alreadyDoneDigitizing[mySel.dataset.index].done = !alreadyDoneDigitizing[mySel.dataset.index].done;
    }
    saveStorageDoneDigitize();
    //console.log(tasks);
    outputDoneDigitize();
}
