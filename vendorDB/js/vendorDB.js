const addVendor= document.querySelector(".addVendorEntry");
const clientTable=document.querySelector("#vendorDatabase");
const vendorDatabase=JSON.parse(localStorage.getItem('Vendors')) || [];

addVendor.addEventListener('submit', addEntry);

outputVendor();

function addEntry(e){
  e.preventDefault();
  const name=this.querySelector("#vendorName").value;
  const link=this.querySelector("#vendorWebsite").value;
  const contact=this.querySelector("#vendorContactName").value;
  const email=this.querySelector("#vendorEmail").value;
  const phone=this.querySelector("#vendorPhone").value;
  const extension=this.querySelector("#vendorExtension").value;
  const addy=this.querySelector("#vendorAddress").value;
  const notes=this.querySelector("#vendorNotes").value
  const newVendor={
    name: name, website:link, contact:contact, email:email, phone:phone,extension:extension, address:addy, notes:notes
  }

  vendorDatabase.push(newVendor);
  saveStorage();
  outputVendor();
  this.reset();
  window.location.reload();
}

function saveStorage(){
  localStorage.setItem('Vendors', JSON.stringify(vendorDatabase));
}

function outputVendor(){
  let html=vendorDatabase.map(function(data, i){
    return '<tr data-index='+i+'><td>'+data.name+'</td><td>'+data.website+'</td><td>'+data.contact+'</td><td><a href="mailto:'+data.email+'">'+data.email+'</a></td><td>'+data.phone+'</td><td>'+data.extension+'</td><td>'+data.address+'</td><td>'+data.notes+'</td><td><button class="deleteEntry"></button></td></tr>'
  })
  clientTable.innerHTML=html.join('');

}


// JSON download to Backup Database
let button = document.querySelector("#exporting");

button.addEventListener('click', function(){

var json = localStorage.Clients;

let dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(json);

let filename = 'Vendors.json';

let elem = document.createElement('a');
elem.setAttribute('href', dataUri);
elem.setAttribute('download', filename);
elem.click();

});

// Delete Entry

const deleteDatabaseEntry=document.querySelectorAll(".deleteEntry");

for(var i = 0; i < deleteDatabaseEntry.length; i++){
   deleteDatabaseEntry[i].addEventListener('click', function(e){

     const myEl=e.target;
     const mySel=myEl.parentElement;
     let index= mySel.parentElement.dataset.index;
     let temp = clientDatabase.splice(index, 1);
     saveStorage();
     outputVendor();
     window.location.reload();
   })
}

// Search Database

function searchVendors(){
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("searchDatabase");
  filter = input.value.toUpperCase();
  table = document.getElementById("vendorDatabase");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

// Upload Backup File

let upload=document.querySelector("#backupFile");

upload.addEventListener('change', handleFileUpload);

let reader=new FileReader();
reader.onload=handleFileRead;

function handleFileUpload(event){
  var file=event.target.files[0];
  reader.readAsText(file);
  console.log(file);
}

function handleFileRead(event){
  let save = JSON.parse(event.target.result) || [];
  window.localStorage.setItem('Vendors', JSON.stringify(save));
  javascript:window.location.reload();
  outputVendor();

}



// Destroy Database

let clear=document.querySelector("#destroyDB");

clear.addEventListener('click', function(){
  javascript:localStorage.clear();
  javascript:window.location.reload();
})
