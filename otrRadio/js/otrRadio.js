

var play = document.getElementById('play');
var audio =  document.getElementById('audioStream');

play.addEventListener('click', function(){
    if (audio.paused){
      audio.play();
    } else {
      audio.pause();
    }
});

        // Custom Volume Controls

var softer = document.getElementById('volumeDown');
var louder = document.getElementById("volumeUp");
var audio = document.getElementById("audioStream");

softer.addEventListener('click', function(){
         audio.volume = .2;
});

louder.addEventListener('click', function(){
  audio.volume = 1;
});

// Change the audio source

var audio = document.getElementById('audioStream');
var stream = document.querySelectorAll('.streamSrc');

  for(var i = 0; i < stream.length; i++){
stream[i].addEventListener('click', function(){

  let playList = this.getAttribute('src');
  audio.setAttribute('src', playList)
}
)};


// Local Storage for User list
     // Player Button to Toggle visbility
var visibility = document.querySelector("#record");
visibility.addEventListener('click', function(){
  var modal1 = document.querySelector(".inputFields");
  modal1.setAttribute('class', 'show-modal');
})




      // Actually adding Stations to User
const userPlaylist = document.querySelector('.submitUserStation');
const myStations = document.querySelector('#newStations');
const userStations = JSON.parse(localStorage.getItem('Stations')) || [];


userPlaylist.addEventListener('submit', addTask);
// myStations.addEventListener('click',toggleDone);
outputStations();



function addTask(e){
    e.preventDefault();
    const name1 = this.querySelector('#name').value;
    const src1 = this.querySelector('#src').value;
    const newLine =
    {
       name: name1, src: src1}


    userStations.push(newLine);

    saveStorage();

    outputStations();
    this.reset();



}

function saveStorage() {
    localStorage.setItem('Stations', JSON.stringify(userStations));
}

function outputStations(){
  let html = userStations.map(function (data, i){

    return '<li><object class="streamSrc" src="' + data.src + '" data-index=' + i + '>' + data.name + '</object></li>';
  })
  myStations.innerHTML = html.join('');

  var audio = document.getElementById('audioStream');
  var stream = document.querySelectorAll('.streamSrc');

    for(var i = 0; i < stream.length; i++){
  stream[i].addEventListener('click', function(){

    let playList = this.getAttribute('src');
    audio.setAttribute('src', playList)
  }
  )};

}

// Toggle Add Station to hidden

  var hidden = document.querySelector("#hide");
  hidden.addEventListener('click', function(){
    var input = document.querySelector(".show-modal");

   input.setAttribute('class', 'inputFields');
  })




// Clear Local saveStorage

let clear = document.querySelector("#erase");

clear.addEventListener('click', function(){
  javascript:localStorage.clear();
  javascript:window.location.reload();
})

// JSON download
let button = document.querySelector("#download");

button.addEventListener('click', function(){

var json = localStorage.Stations;

let dataUri = 'data:application/json;charset=utf-8,' + encodeURIComponent(json);

let filename = 'Stations.json';

let elem = document.createElement('a');
elem.setAttribute('href', dataUri);
elem.setAttribute('download', filename);
elem.click();

});

// Upload User JSON File into Local Storage
    // Toggle visibility
var jsonToggle = document.querySelector("#upload");

jsonToggle.addEventListener('click', function(){

  var modal1 = document.querySelector(".uploadField");
  modal1.setAttribute('class', 'show-modal1');
})

// Actual Uploading here
let upload = document.querySelector("#jsonUpload");


upload.addEventListener("onchange", handleFileUpload);

let reader = new FileReader();
reader.onload = handleFileRead;

function handleFileUpload(event){

var file = event.target.files[0];
reader.readAsText(file);
}

function handleFileRead(event){
  let save = JSON.parse(event.target.result) || [];
  window.localStorage.setItem('Stations', JSON.stringify(save));
  javascript:window.location.reload();
  outputStations();


}

// Toggle Visibility of Upload json

var uploadHide = document.querySelector("#uploadHidden");

uploadHide.addEventListener('click', function(){
  var input = document.querySelector(".show-modal1");

 input.setAttribute("class", "uploadField");
})
