# otrDB

An electron application for Old Time Radio collectors.

# Purpose

otrDB main goal was to port Old Time Radio Researcher's Otter program's log database to a modern cross platform program.  It also comes with a handy note application and streaming radio program (hardcoded with some OTR centric streaming stations) that also has the ability for the user to enter their own stations as well.  The stations are not endorsed by this project, just there for entertainment value only.

# Logs

If you would just like to contribute to the logs alone, you can easily get to that section by following this link here: [otrDB Logs](https://github.com/wwderw/otrDB/tree/master/logs/logs).  Each series log is in their own CSV file to make contributions easily handled without having to worry about keeping up with database versions.

If you do not see a log for the show you are interested in, submit a pull request for the addition of your CSV file.  Please be sure to follow the schema of the other CSV files as well.


