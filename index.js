
const { app, BrowserWindow, shell, Menu } = require("electron");
Menu.setApplicationMenu(false);

let mainWindow = null;

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 1980,
        height: 1080,
        useContentSize: true,
        title: "otrDB",
        backgroundColog: '#ffffff'

    });




    mainWindow.loadFile("otrDB.html");

    mainWindow.on("closed", function() {

        mainWindow = null;

    });

    const mainMenu = Menu.buildFromTemplate(menuTemplate)
    Menu.setApplicationMenu(mainMenu)

}


app.on("ready", createWindow);



// Quit when all windows are closed.
app.on("window-all-closed", function() {
    app.quit();
});

// Menu menuTemplate

const menuTemplate = [
    {
      label: 'Menu',
      submenu: [
        {label:'Client Database', click(){clientDB()},
      accelerator: 'Ctrl+C'},
        {label: 'otrDB', click(){otrDB()},
      accelerator: 'Ctrl+D'},
        {label: 'otrRadio Streaming Player', click(){otrRadio()},
      accelerator: 'Ctrl+P'},
        {label:'Vendor Database', click(){vendorDB()},
      accelerator: 'Ctrl+V'},
        {type: 'separator'},
        {label: 'Quit', click(){app.quit()}}
      ]
    },
    {label: 'About',
     submenu: [
       {label: 'About otrDB', click(){about()}}
     ]
  }
]

// Client Database
function clientDB(){
  clientdbWindow = new BrowserWindow({
     width: 1980,
     height: 1080,
     useContentSize: true,
     title: "otrDB - Client Database",
     backgroundColor: '#ffffff',
     autoHideMenuBar: true,
     parent: mainWindow,

  });

  clientdbWindow.loadFile("clientDB/clientDB.html");
  clientdbWindow.on("closed", function() {

  clientdbWindow = null;

  });
};

// OTR Database
function otrDB(){
databaseWindow = new BrowserWindow({
   width: 600,
   height: 600,
   maxWidth: 600,
   maxHeight: 600,
   useContentSize: true,
   title: "otrDB - Database",
   backgroundColor: '#ffffff',
   autoHideMenuBar: true,
   parent: mainWindow
});

databaseWindow.loadFile("logs/mainLogs.html");
databaseWindow.on("closed", function() {

databaseWindow = null;

});
};


// otrDB Radio Player

function otrRadio(){

  otrRadioWindow = new BrowserWindow({
     width: 600,
     height: 600,
     maxWidth: 600,
     maxHeight: 600,
     useContentSize: true,
     title: "otrDB Radio Player",
     backgroundColor: '#ffffff',
     autoHideMenuBar: true,
     parent: mainWindow
  });

  otrRadioWindow.loadFile("otrRadio/otrRadio.html");
  otrRadioWindow.on("closed", function() {

   otrRadioWindow = null;

  });


};

// Vendor Database

function vendorDB(){
  vendordbWindow = new BrowserWindow({
     width: 1980,
     height: 1080,
     useContentSize: true,
     title: "otrDB - Vendor Database",
     backgroundColor: '#ffffff',
     autoHideMenuBar: true,
     parent: mainWindow
  });

  vendordbWindow.loadFile("vendorDB/vendorDB.html");
  vendordbWindow.on("closed", function() {

   vendordbWindow = null;
});

};

// otrDB About info

function about(){

  aboutWindow = new BrowserWindow({
     width: 600,
     height: 700,
     useContentSize: true,
     title: "About otrDB",
     backgroundColor: '#ffffff',
     autoHideMenuBar: true,
     frame: false,
     parent: mainWindow,
     icon: path.join(__dirname, 'assets/icons/otrDBLogo.png')
  });

  aboutWindow.loadFile("about/about.html");
  aboutWindow.on("closed", function() {

   aboutWindow = null;

  });

};
